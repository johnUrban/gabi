#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import math
import os
import scipy.stats
import subprocess
import tempfile

from Bio import SeqIO
from random import random

from biolite import utils
from biolite.config import get_resource


class Sample:

    def __init__(self, likelihood, prior, length, ncontigs):
        self.odds = likelihood + prior
        self.likelihood = likelihood
        self.prior = prior
        self.length = length
        self.ncontigs = ncontigs

    def odds_ratio(self, current):
        """
        """
        if math.isinf(self.odds):
            if math.isinf(current.odds):
                if not (math.isinf(self.likelihood) or math.isinf(current.likelihood)):
                    return((self.likelihood - current.likelihood) > math.log(random()))
                else:
                    return(random() > 0.5)
            else:
                return False
        elif math.isinf(current.odds):
            return True
        else:
            return((self.odds - current.odds) > math.log(random()))


class PriorDistribution:

    def __init__(self, scale, length, ncontigs):
        """
        Create a prior distribution from the estimated genome size
        and number of contigs.
        """
        scale = float(scale)
        self.length_pdf = scipy.stats.gamma(scale, scale=length/scale).pdf
        self.ncontigs_pdf = scipy.stats.gamma(scale, scale=ncontigs/scale).pdf

    def log_prob(self, length, ncontigs):
        p1 = self.length_pdf(length)
        p2 = self.ncontigs_pdf(ncontigs)
        if p1 <= 0 or math.isnan(p1) or p2 <= 0 or math.isnan(p2):
            return float("-inf")
        else:
            return math.log(p1) + math.log(p2)


class Sampler:

    def __init__(
        self, reads, length, ncontigs, scale, insert_mean, insert_stdev,
        nodata=False, flatprior=False):

        if nodata:
            self.reads = None
        else:
            self.reads = reads
            fq = SeqIO.parse(reads[0], "fastq")
            readlen = len(next(fq).seq)
            utils.info("detected read length:", readlen)
            nreads = 1 + sum(1 for record in fq)
            utils.info("detected # of reads:", nreads)
            self.coverage = readlen * nreads / length
            utils.info("estimated coverage:", self.coverage)
            utils.info("setting LAP threshold to 1e-%d" % self.coverage)

        if flatprior:
            self.prior = (lambda length, ncontigs : 1.0)
        else:
            self.prior = PriorDistribution(scale, length, ncontigs).log_prob

        self.threads = get_resource('threads')
        self.insert_mean = insert_mean
        self.insert_stdev = insert_stdev

        self.tmpdir = tempfile.mkdtemp()
        utils.info("created tmpdir for Bowtie2 calls:", self.tmpdir)

        self.logfile = os.path.join(self.tmpdir, "lap.log")
        self.dbfile = os.path.join(self.tmpdir, "assembly")
        self.samfile = os.path.join(self.tmpdir, "lap.sam")

    def likelihood(self, filename):
        """
        Estimate assembly likelihood with LAP.
        """
        if self.reads:
            with open(self.logfile, 'w') as f:
                try:
                    subprocess.check_call(
                        [ "bowtie2-build", filename, self.dbfile ],
                        stdout=f, stderr=f)
                    subprocess.check_call(
                        [
                            "bowtie2", "--very-sensitive", "--sam-no-hd",
                            "--reorder", "--no-mixed", "-k", "10000",
                            "-p", self.threads, "-x", self.dbfile,
                            "-1", self.reads[0], "-2", self.reads[1],
                            "-I", '%.0f' % (self.insert_mean - 2*self.insert_stdev),
                            "-X", '%.0f' % (self.insert_mean + 2*self.insert_stdev),
                            "-S", self.samfile
                        ],
                        stdout=f, stderr=f)
                    lap = subprocess.check_output(
                        'calc_prob.py -s {} -a {} -1 {} -2 {} -o fr -m {} -t {} -X {} -I {} | sum_prob.py -t 1e-{}'.format(
                            self.samfile, filename, self.reads[0], self.reads[1],
                            int(self.insert_mean), int(self.insert_stdev),
                            int(self.insert_mean + 2*self.insert_stdev),
                            int(self.insert_mean - 2*self.insert_stdev),
                            self.coverage),
                        shell=True, stderr=f)
                    return float(lap.split()[0])
                except subprocess.CalledProcessError as e:
                    utils.die(e, "\nfailed to call Bowtie2/LAP: check the logfile", self.logfile)
        else:
            return 1.0

    def sample(self, contigs, filename):
        """
        Run CGAL wrapper script to calculate log-likelihood of the contigs
        given the reads.
        """
        # Write out contigs to FASTA file.
        length = 0
        ncontigs = 0
        with open(filename, 'w') as f:
            for contig in contigs:
                SeqIO.write(contig, f, 'fasta')
                length += len(contig.seq)
                ncontigs += 1
        return Sample(
            self.likelihood(filename), self.prior(length, ncontigs),
            length, ncontigs)

