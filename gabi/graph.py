#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import ast
import networkx as nx

from itertools import izip
from random import randint

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

# Parameters for drawing graphviz dot plots
dot_graph_size = (10, 10)
dot_max_node_size = 1.0
dot_min_node_size = 0.1
dot_palette = (
    (186,186,186),
    (253,174,97),
    (215,25,28))

def colormap(palette, x):
    l = len(palette) - 1
    x = float(x) * l
    lo = int(x)
    if lo < l:
        mid = x - lo
        hi = lo + 1
        rgb = (
            int(palette[lo][0] + mid * (palette[hi][0] - palette[lo][0])),
            int(palette[lo][1] + mid * (palette[hi][1] - palette[lo][1])),
            int(palette[lo][2] + mid * (palette[hi][2] - palette[lo][2])))
    else:
        rgb = palette[-1]
    return '#%02x%02x%02x' % rgb

def choice(x):
    if not x:
        return None
    else:
        return x[randint(0, len(x) - 1)]

# Graph class
class Graph:
    def __init__(self, graphml):
        self.G = nx.read_graphml(graphml, node_type=int)
        self.N = self.G.number_of_nodes()
        self.E = self.G.number_of_edges()
        self.A = nx.DiGraph() # assembly graph
        # enumerate the edges
        self.edges = []
        for e in self.G.edges_iter():
            self.G.edge[e[0]][e[1]]['id'] = len(self.edges)
            self.edges.append(e)
        # determine the kmer length used to construct the graph
        self.k = len(self.G.node[next(self.G.nodes_iter())]['kmer'])

    # Graph accessors

    def seq(self, n):
        return self.G.node[n]['seq']

    def kmers(self):
        return dict((self.G.node[n]['kmer'], n) for n in self.G.nodes_iter())

    def edge(self, n1, n2):
        if self.G.has_edge(n1, n2):
            return self.G.edge[n1][n2]['id']
        else:
            return None

    def edge_random(self):
        return randint(0, self.E - 1)

    def forward(self, n):
        return self.G.successors_iter(n)

    def backward(self, n):
        return self.G.predecessors_iter(n)

    def forward_random(self, n):
        return choice(self.G.successors(n))

    def backward_random(self, n):
        return choice(self.G.predecessors(n))

    def forward_active(self, n):
        if self.A.has_node(n) and self.A.out_degree(n):
            return next(self.A.successors_iter(n))
        else:
            return None

    def backward_active(self, n):
        if self.A.has_node(n) and self.A.in_degree(n):
            return next(self.A.predecessors_iter(n))
        else:
            return None

    # Graph mutators

    def reset_visited(self, active=False):
        if active:
            nodes = self.A.nodes_iter()
        else:
            nodes = self.G.nodes_iter()
        for n in nodes:
            self.G.node[n]['visited'] = False

    def visited(self, n, val=None):
        if val is not None:
            self.G.node[n]['visited'] = val
        return self.G.node[n]['visited']

    def activate(self, e):
        self.A.add_edge(*self.edges[e])

    def deactivate(self, e):
        n1, n2 = self.edges[e]
        self.A.remove_edge(n1, n2)
        if not self.A.in_degree(n1):
            self.A.remove_node(n1)
        if not self.A.out_degree(n2):
            self.A.remove_node(n2)

    # Graph output

    def contig_header(self, path):
        header = []
        for e in path:
            header.append('#node%d' % self.tail(e))
            header.append('#edge%d' % e)
        return ','.join(header)

    def contig_record(self, path, i, desc=""):
        """
        Build a FASTA SeqRecord for the `path` through the graph.
        """
        seq = ''.join(self.G.node[n]['seq'] for n in path)
        return SeqRecord(
            Seq(seq), 'contig-%d' % i, description=str(len(seq))+desc)

    def contigs(self, records=True, paths=False):
        """
        An iterator that yields a FASTA SeqRecord for each path in the
        assembly graph.
        """
        ncontigs = 0
        self.reset_visited(active=True)
        # First, find all linear contigs starting at a node with an out-edge
        # but no in-edge.
        for n in self.A.nodes_iter():
            if self.A.in_degree(n) == 0:
                path = []
                while n is not None:
                    assert self.visited(n) == False, path + [n]
                    self.visited(n, True)
                    path.append(n)
                    n = self.forward_active(n)
                if records:
                    yield self.contig_record(path, ncontigs)
                elif paths:
                    yield path
                else:
                    yield self.contig_header(path)
                ncontigs += 1
        # Next, find all circular contigs from the remaining nodes.
        for start in self.A.nodes_iter():
            if not self.visited(start):
                self.visited(start, True)
                path = [start]
                n = self.forward_active(start)
                while not (n is None or n == start):
                    assert self.visited(n) == False, path + [n]
                    self.visited(n, True)
                    path.append(n)
                    n = self.forward_active(n)
                if records:
                    yield self.contig_record(path, ncontigs)
                elif paths:
                    yield path
                else:
                    yield self.contig_header(path)
                ncontigs += 1

    def contigs_annotated(self, frequencies):
        # Reset assembly graph.
        self.A = nx.DiGraph()
        for id in frequencies:
            if id.startswith("node"):
                self.A.add_node(int(id[4:]))
            elif id.startswith("edge"):
                self.activate(int(id[4:]))
        for i, path in enumerate(self.contigs(records=False, paths=True)):
            posteriors = []
            j = 0
            for n in path:
                l = len(self.seq(n))
                posteriors.append('%d-%d: %f' % (j, j+l-1, frequencies["node%d" % n]))
                j += l
            yield self.contig_record(
                path, i, desc=" posteriors=[%s]" % ', '.join(posteriors))

    def write_dot(self, filename, frequencies=None, colors=None, active=False):
        """
        Write a dot file for the graph, highlighting the selected nodes and
        edges.
        """
        max_width = max(len(self.G.node[n]['seq']) for n in self.G.nodes_iter())
        norm = (dot_max_node_size - dot_min_node_size) / max_width
        with open(filename, 'w') as f:
            print >>f, 'graph G {'
            print >>f, 'splines=false;'
            for n in self.G.nodes_iter():
                id = "node%d" % n
                width = norm * len(self.G.node[n]['seq']) + dot_min_node_size
                if frequencies:
                    color = 'color="%s"' % colormap(dot_palette, frequencies.get(id, 0.0))
                elif colors:
                    color = "color=%s" % colors.get(id, "grey70")
                elif active:
                    if self.A.has_node(n):
                        color = "color=red"
                    else:
                        color = "color=grey70"
                else:
                    color = "color=grey70"
                print >>f, '"%d" [id="%s" label="" shape=circle penwidth=4.0 %s width=%f];' % (n, id, color, width)
            for n1, n2 in self.G.edges_iter():
                id = "edge%d" % self.G.edge[n1][n2]['id']
                if frequencies:
                    color = 'color="%s"' % colormap(dot_palette, frequencies.get(id, 0.0))
                elif colors:
                    color = "color=%s" % colors.get(id, "grey70")
                elif active:
                    if self.A.has_edge(n1, n2):
                        color = "color=red"
                    else:
                        color = "color=grey70"
                else:
                    color = "color=grey70"
                print >>f, '"%d" -- "%d" [id="%s" %s penwidth=2.0];' % (n1, n2, id, color)
            print >>f, '}'

    # Graph state

    def set_state(self, edges_str, nodes_str):
        # reset the assembly graph
        self.A = nx.DiGraph()
        # activate each node in the state
        map(self.activate, ast.literal_eval(edges_str))
        # verify node list
        nodes = ast.literal_eval(nodes_str)
        assert len(nodes) == self.A.number_of_nodes()
        for n1, n2 in izip(nodes, self.get_nodes()):
            assert n1 == n2

    def get_nodes(self):
        return sorted(self.A.nodes_iter())

    def get_edges(self):
        return sorted(self.edge(*e) for e in self.A.edges_iter())

    def get_state(self):
        return str(self.get_edges()), str(self.get_nodes())

    def perturb_simple(self):
        e = self.edge_random()
        n1, n2 = self.edges[e]
        if self.A.has_edge(n1, n2):
            self.deactivate(e)
        else:
            if self.A.has_node(n1) and self.A.out_degree(n1):
                self.A.remove_node(n1)
            if self.A.has_node(n2) and self.A.out_degree(n2):
                self.A.remove_node(n2)
            self.A.add_edge(n1, n2)

    def perturb_path(self):
        e = self.edge_random()
        n1, n2 = self.edges[e]
        if self.A.has_edge(n1, n2):
            self.deactivate(e)
        else:
            path = []
            # Search forward until reaching an active path or an endpoint
            n = n2
            while not (n is None or self.A.has_node(n)):
                path.append(n)
                n = self.forward_random(n)
            end = n
            # Search backward until reaching an active path or an endpoint
            n = n1
            while not (n is None or self.A.has_node(n)):
                path.insert(0, n)
                n = self.backward_random(n)
            start = n
            # Deactivate old path forward
            if start is not None:
                assert self.A.has_node(start)
                n = self.forward_active(start)
                while not (n is None or n == end):
                    n1 = self.forward_active(n)
                    self.A.remove_node(n)
                    n = n1
                path.insert(0, start)
            # Deactivate old path backward
            if end is not None:
                assert self.A.has_node(end)
                n = self.backward_active(end)
                while not (n is None or n == start):
                    n1 = self.backward_active(n)
                    self.A.remove_node(n)
                    n = n1
                path.append(end)
            # Activate new path
            self.A.add_path(path)
        return e

    def randomize_state(self, length=None):
        for _ in xrange(self.E):
            e = self.edge_random()
            n1, n2 = self.edges[e]
            if self.A.has_node(n1) and self.A.out_degree(n1):
                continue
            if self.A.has_node(n2) and self.A.in_degree(n2):
                continue
            self.activate(e)
            if length and sum(map(len, map(self.seq, self.A.nodes_iter()))) > length:
                break
        self.write_dot('random.dot', active=True)

