#!/bin/bash
#SBATCH -c 8
#SBATCH -t 10:00:00
#SBATCH --mem=8G
#SBATCH -C e5000
#SBATCH --exclusive

set -e

export BIOLITE_RESOURCES="database=$PWD/biolite.sqlite,outdir=$PWD/analyses,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

gabi sample --id phix --fastq $PWD/subset.?.fq --graphml $PWD/cluster0.graphml --samples 20000 --contigs 1 --genome_size 5386 --insert_mean 357 --insert_stdev 41 --perturbations 3 --seed 117531730804430877177666184607033490730

